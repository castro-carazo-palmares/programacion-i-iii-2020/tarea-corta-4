package Negocio;

import Datos.Estudiante;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Escritura {
    private BufferedWriter escritor;

    public void abrirArchivo() {
        try {
            escritor = new BufferedWriter(new FileWriter("estudiantes.txt"));
        } catch (IOException ex) {
            System.err.println("Error al crear el archivo");
        }
    }

    public void escribirArchivo(Estudiante estudiante) {
        try {
            escritor.write(estudiante.guardar());
        } catch (IOException e) {
            System.err.println("Error al escribir en el archivo");
        }
    }

    public void cerrarArchivo() {
        try {
            if(escritor != null) {
                escritor.close();
            }
        } catch (IOException e) {
            System.err.println("Error al cerrar el archivo");
        }
    }

}
