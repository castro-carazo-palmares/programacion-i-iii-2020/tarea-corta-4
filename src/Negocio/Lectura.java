package Negocio;

import Datos.Estudiante;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Lectura {
    private BufferedReader lector;

    public void abrirArchivo() {
        try {
            lector = new BufferedReader(new FileReader("estudiantes.txt"));
        } catch (FileNotFoundException e) {
            System.err.println("Error al encontrar el archivo");
        }
    }

    public Estudiante[] leerArchivo() {
        Estudiante[] vector = new Estudiante[100];
        int contadorVector = 0;
        try {
            Estudiante estudiante;
            String linea = lector.readLine();
            String[] datos;
            int contador;

            while(linea != null) {
                estudiante = new Estudiante();
                contador = 0;
                datos = linea.split(",");
                estudiante.setCedula(Integer.parseInt(datos[contador++]));
                estudiante.setNombre(datos[contador++]);
                estudiante.setPrimerApellido(datos[contador++]);
                estudiante.setSegundoApellido(datos[contador++]);
                estudiante.setTelefono(Integer.parseInt(datos[contador]));
                vector[contadorVector] = estudiante;
                contadorVector++;

                linea = lector.readLine();
            }
        } catch (IOException e) {
            System.err.print("Error al leer el archivo");
        }

        return vector;
    }

    public void cerrarArchivo() {
        try {
            if(lector != null) {
                lector.close();
            }
        } catch (IOException e) {
            System.err.println("Error al cerrar el archivo");
        }
    }
}
