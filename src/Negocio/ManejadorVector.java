package Negocio;

import Datos.Estudiante;

public class ManejadorVector {
    private int contador = 0;
    static Estudiante[] vector = new Estudiante[100];

    public ManejadorVector() {
        Lectura lector = new Lectura();

        lector.abrirArchivo();
        vector = lector.leerArchivo();
        lector.cerrarArchivo();

        for (int i = 0; i < vector.length; i++) {
            if(vector[i] != null) {
                contador++;
            }
        }
    }

    public boolean estaLleno() {
        return contador == vector.length;
    }

    public boolean insertarEstudiante(Estudiante estudiante) {
        boolean seInserto = false;

        if(!estaLleno()) {
            vector[contador] = estudiante;
            seInserto = true;
            contador++;
        }

        return seInserto;
    }

    public String imprimirVector() {
        String texto = "";

        for (int i = 0; i < vector.length; i++) {
            if(vector[i] != null) {
                texto += vector[i].toString() + "\n";
            }
        }

        return texto;
    }

    public Estudiante[] getVector() {
        return vector;
    }

}
