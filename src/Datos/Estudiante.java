package Datos;

public class Estudiante {
    private int cedula;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private int telefono;

    public Estudiante() {
        this(0,"","","",0);
    }

    public Estudiante(int cedula, String nombre, String primerApellido, String segundoApellido, int telefono) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.telefono = telefono;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Cédula: " + cedula + "\n" +
                "Nombre: " + nombre + "\n" +
                "Primer Apellido: " + primerApellido + "\n" +
                "Segundo Apellido: " + segundoApellido + "\n" +
                "Teléfono: " + telefono + "\n";
    }

    public String guardar() {
        return cedula + "," + nombre + "," + primerApellido + "," + segundoApellido + "," + telefono + "\n";
    }
}
