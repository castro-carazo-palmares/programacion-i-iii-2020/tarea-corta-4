package Presentacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Lista {
    private JPanel panel;
    private JTextArea txtLista;
    private JButton btnRegresar;

    public Lista() {
        JFrame frame = new JFrame("Lista");
        frame.setLayout(null);
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setSize(500, 540);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        panel.setLayout(null);
        frame.setVisible(true);

        txtLista.setEditable(false);
        //txtLista.setBounds(20, 20, 460, 400);
        btnRegresar.setBounds(200, 440, 100, 50);

        JScrollPane scrollPane = new JScrollPane(txtLista);
        scrollPane.setBounds(20, 20, 460, 400);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        panel.add(scrollPane);

        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new Programa();
            }
        });

        txtLista.setText(Programa.manejador.imprimirVector());
    }
}
