package Presentacion;

import Negocio.Escritura;
import Negocio.ManejadorVector;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class Programa {
    private JPanel panel;
    private JButton btnIngresarEstudiante;
    private JButton btnListarEstudiantes;
    private JButton btnSalir;
    static ManejadorVector manejador = new ManejadorVector();

    public Programa() {
        JFrame frame = new JFrame("Programa");
        frame.setLayout(null);
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setSize(660, 250);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        panel.setLayout(null);

        File archivo = new File("logo.png");
        JLabel imgLabel = new JLabel(new ImageIcon(archivo.getName()));

        imgLabel.setBounds(20, 35,400, 152);
        btnIngresarEstudiante.setBounds(440, 30, 200, 50);
        btnListarEstudiantes.setBounds(440, 90, 200, 50);
        btnSalir.setBounds(440, 150, 200, 50);
        panel.add(imgLabel);

        frame.setVisible(true);

        btnIngresarEstudiante.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new Informacion();
            }
        });

        btnListarEstudiantes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new Lista();
            }
        });

        btnSalir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Escritura escritor = new Escritura();
                escritor.abrirArchivo();
                for (int i = 0; i < manejador.getVector().length; i++) {
                    if(manejador.getVector()[i] != null) {
                        escritor.escribirArchivo(manejador.getVector()[i]);
                    }
                }
                escritor.cerrarArchivo();
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        new Programa();
    }
}
