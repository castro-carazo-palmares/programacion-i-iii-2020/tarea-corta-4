package Presentacion;

import Datos.Estudiante;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Informacion {
    private JPanel panel;
    private JTextField txtCedula;
    private JTextField txtNombre;
    private JTextField txtPrimerApellido;
    private JTextField txtSegundoApellido;
    private JTextField txtTelefono;
    private JButton btnIngresar;
    private JButton btnRegresar;
    private JLabel lblCedula;
    private JLabel lblNombre;
    private JLabel lblPrimerApellido;
    private JLabel lblSegundoApellido;
    private JLabel lblTelefono;

    public Informacion() {
        JFrame frame = new JFrame("Informacion");
        frame.setLayout(null);
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setSize(480, 450);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        panel.setLayout(null);
        frame.setVisible(true);

        lblCedula.setBounds(60, 20, 150, 40);
        lblNombre.setBounds(60, 80, 150, 40);
        lblPrimerApellido.setBounds(60, 140, 150, 40);
        lblSegundoApellido.setBounds(60, 200, 150, 40);
        lblTelefono.setBounds(60, 260, 150, 40);

        txtCedula.setBounds(210, 20, 210, 40);
        txtNombre.setBounds(210, 80, 210, 40);
        txtPrimerApellido.setBounds(210, 140, 210, 40);
        txtSegundoApellido.setBounds(210, 200, 210, 40);
        txtTelefono.setBounds(210, 260, 210, 40);

        btnIngresar.setBounds(75, 330, 160, 50);
        btnRegresar.setBounds(245, 330, 160, 50);

        if(Programa.manejador.estaLleno()) {
            btnIngresar.setEnabled(false);
        }

        btnIngresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (validarCampos()) {
                    int cedula = Integer.parseInt(txtCedula.getText().trim());
                    String nombre = txtNombre.getText().trim();
                    String primerApellido = txtPrimerApellido.getText().trim();
                    String segundoApellido = txtSegundoApellido.getText().trim();
                    int telefono = Integer.parseInt(txtTelefono.getText().trim());

                    Estudiante estudiante = new Estudiante(cedula, nombre, primerApellido, segundoApellido, telefono);

                    boolean seInserto = Programa.manejador.insertarEstudiante(estudiante);

                    if(seInserto) {
                        JOptionPane.showMessageDialog(null, "El estudiante se insertó correctamente");
                        limpiarCampos();
                    } else {
                        JOptionPane.showMessageDialog(null, "El vector está lleno");
                    }

                    if(Programa.manejador.estaLleno()) {
                        btnIngresar.setEnabled(false);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Por favor complete todos los datos");
                }
            }
        });

        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                new Programa();
            }
        });
    }

    public void limpiarCampos() {
        txtCedula.setText("");
        txtNombre.setText("");
        txtPrimerApellido.setText("");
        txtSegundoApellido.setText("");
        txtTelefono.setText("");
        txtCedula.requestFocus();
    }

    public boolean validarCampos() {
        return !txtCedula.getText().equals("") &&
                !txtNombre.getText().equals("") &&
                !txtPrimerApellido.getText().equals("") &&
                !txtSegundoApellido.getText().equals("") &&
                !txtTelefono.getText().equals("");
    }
}
